public class Main {
    public static void main(String[] args) {
    try {
        int[] array1 = {1, 2, 3};
        System.out.println(array1[3]);
        }
       catch (ArrayIndexOutOfBoundsException e){
           System.out.println("This is why QA Engineers always have to do boundary testing! The");
           System.out.println("array only has 3 values and you've requested a 4th.");
        }
        catch (Exception e){
            System.out.println("Something went wrong!!");
        }
    }
}